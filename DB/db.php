<?php
class DBconnection {
        private $dbconn;
        function __construct($servername, $username, $password, $database) {
            $this->createConnection($servername, $username, $password, $database);
        }
        function createConnection($servername, $username, $password, $database) { //vytvorili sme si pripojenie do databazy
            $this->dbconn = new mysqli($servername, $username, $password, $database);
            if ($this->dbconn->connect_error) {
                die("Connection failed: " . $this->dbconn->connect_error);
            }
        }
        function getDB() {
            return $this->dbconn;
        }
        function closeConnection() {
            $this->dbconn->close();
        }

        // validacia
        function filterEmpty($name, $data, &$errorSTR) {
            if(empty($data)) {
                $errorSTR = $name . " je prazdny.";
               return true;
            }
            $errorSTR = "";
            return false;
        }

        function filterCena($name, $data, &$errorSTR) {
            if($this->filterEmpty($name, $data,$errorSTR)) {
                return true;
            }
            if(!filter_var((float)$data, FILTER_VALIDATE_FLOAT)) {
                $errorSTR = $name . " nie je cislo.";
                return true;
            }
            $errorSTR = "";
            return false;
        }

        function filterObrazok($name, $data, &$errorSTR) {
            if($this->filterEmpty($name, $data,$errorSTR)) {
                return true;
            }
            if(!filter_var($data, FILTER_VALIDATE_URL)) {
                $errorSTR = $name . " nie je vadilna url.";
                return true;
            }
            $errorSTR = "";
            return false;
        }

    }
?>