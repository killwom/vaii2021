<?php
    require_once('db.php');
    class DBOblecenie extends DBconnection {
        function __construct($servername, $username, $password, $database)
        {
            parent::__construct($servername, $username, $password, $database);
        }

        // vrati vsetko oblecenie
        function getOblecenie() {
            $dbConn = parent::getDB();
            $query = "SELECT * FROM oblecenie";
            return $dbConn->query($query);
        }

        // vrati jedno oblecenie podla id
        function getOblecenieByID($id) {
            $dbConn = parent::getDB();
            $query = "SELECT * FROM oblecenie WHERE id=" . $id;
            return $dbConn->query($query);
        }

        // prida oblecenie
        function addOblecenie($data) {
            $nazov = $data['nazov'];
            $cena = $data['cena'];
            $popis = $data['popis'];
            $imgURL = $data['imgURL'];

            $dbConn = parent::getDB();
            $query = "INSERT INTO oblecenie (nazov, cena, obrazok, popis) VALUES ('$nazov', '$cena', '$imgURL', '$popis')";
            return $dbConn->query($query);
        }

        // upravi oblecenie podla id
        function editOblecenie($data) {
            $nazov = $data['nazov'];
            $cena = $data['cena'];
            $popis = $data['popis'];
            $imgURL = $data['imgURL'];
            $id = $data['id'];

            $dbConn = parent::getDB();
            $query = "UPDATE oblecenie SET nazov='$nazov', cena='$cena', obrazok='$imgURL', popis='$popis' WHERE id='$id'";
            return $dbConn->query($query);
        }

        // vymaze oblecenie podla id
        function deleteOblecenie($id) {
            $dbConn = parent::getDB();
            $query = "DELETE FROM oblecenie WHERE id='$id'";
            return $dbConn->query($query);
        }
    }
