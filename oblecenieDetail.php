<?php
// php includes
require_once('DB/dbOblecenie.php');
// html includes
include_once('includes/header.html');
include_once('includes/navigation.html');
// vytvorenie DB spojenia spolu s funkcionalitou pre oblecenie
$db = new DBOblecenie("localhost", "root", "", "semestralka");

$id = $_GET['idOblecenia'];
$oblecenie = $db->getOblecenieByID($id);
?>
    <ul class="oblecenie-detail">
        <?php
        if($oblecenie->num_rows == 1) {
            while($item = $oblecenie->fetch_assoc()) {
                echo "<li>
                    <img src='" . $item['obrazok'] . "'>
                    <h1>" . $item['nazov'] . " </h1>
                    <h2>" . $item['cena'] . " €</h2>
                    <p>" . $item['popis'] . "</p>
                    <button>Pridat do kosika</button>
                </li>";
            }
        } else {
            echo "<h1>Ziadne oblecenie sa nenaslo!</h1>";
        }
        ?>
    </ul>
<?php
include_once('includes/footer.html');
?>