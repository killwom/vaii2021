<?php
// php includes
    require_once('DB/dbOblecenie.php');
// html includes
    include_once('includes/header.html');
    include_once('includes/navigation.html');
    // vytvorenie DB spojenia spolu s funkcionalitou pre oblecenie
$db = new DBOblecenie("localhost", "root", "", "semestralka");
// vrati vsetko oblecenie z db
$oblecenie = $db->getOblecenie();
?>
<div class="container">
    <p>Vítajte na e-shope TopLook, kde nájdete rôzne druhy oblečenia, či sa už jedná o zimné alebo letné.
        Ponúkame Vám oblečenie tej najlepšej kvality pre dámy a pánov. Neváhajte a objednajte si u nás to najlepšie pre Vás
        alebo vašich blízkych. Veríme, že sa Vám naše produkty budú páčiť. A aby sme nezabudli, taktiež budete pravideľne
        informovaný o akciách a súťažiach, v ktorých budete mať jedinečnú šancu vyhrať nákupnú kartu s logom našej firmy,
        na ktorej bude hodnota až 300€!
    </p>
</div>
<ul class="oblecenie-list">
    <?php
        if($oblecenie->num_rows > 0) {
            while($item = $oblecenie->fetch_assoc()) {
                echo "<li>
                    <a href='oblecenieDetail.php?idOblecenia=" . $item['id'] . "'>
                        <img src='" . $item['obrazok'] . "'>
                        <h1>" . $item['nazov'] . " </h1>
                        <h2>" . $item['cena'] . " €</h2>
                        <p>" . $item['popis'] . "</p>
                    </a>
                </li>";
            }
        } else {
            echo "<h1>Ziadne oblecenie sa nenaslo!</h1>";
        }
    ?>
</ul>
<?php
    include_once('includes/footer.html');
?>