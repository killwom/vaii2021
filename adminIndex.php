<?php
// php includes
require_once('DB/dbOblecenie.php');
// html includes
include_once('includes/header.html');
include_once('includes/navigation.html');
// vytvorenie DB spojenia spolu s funkcionalitou pre oblecenie
$db = new DBOblecenie("localhost", "root", "", "semestralka");
$oblecenie = $db->getOblecenie();
?>
<h1>Admin panel</h1>
    <a class="nav-link" href="addOblecenie.php">Pridat oblecenie</a>
    <ul class="oblecenie-list">
        <?php
        if($oblecenie->num_rows > 0) {
            while($item = $oblecenie->fetch_assoc()) {
                echo "<li>
                    <a href='oblecenieDetail.php?idOblecenia=" . $item['id'] . "'>
                        <img src='" . $item['obrazok'] . "'>
                        <h1>" . $item['nazov'] . " </h1>
                        <h2>" . $item['cena'] . " €</h2>
                        <p>" . $item['popis'] . "</p>
                    </a>
                    <a href='editOblecenie.php?idOblecenia=" . $item['id'] . "'>Upraviť</a>
                    <form action='deleteOblecenie.php' method='post'>
                        <input type='hidden' name='idObleceniaDelete' value='" . $item['id'] . "'>
                        <button type='submit'>Odstrániť</button>
                    </form>
                   
                </li>";
            }
        } else {
            echo "<h1>Ziadne oblecenie sa nenaslo!</h1>";
        }
        ?>
    </ul>
<?php
include_once('includes/footer.html');
?>