<?php
// php includes
require_once('DB/dbOblecenie.php');
// html includes
include_once('includes/header.html');
include_once('includes/navigation.html');
// vytvorenie DB spojenia spolu s funkcionalitou pre oblecenie
$db = new DBOblecenie("localhost", "root", "", "semestralka");

//errors
$errorNazov = false;
$errorCena = false;
$errorPopis = false;
$errorObrazok = false;

$errorNazovSTR = "";
$errorCenaSTR = "";
$errorPopisSTR = "";
$errorObrazokSTR = "";

if(isset($_POST['nazov']) && isset($_POST['cena']) && isset($_POST['popis']) && isset($_POST['imgURL'])) {
    // kontrola - validacia
    $errorNazov = $db->filterEmpty("nazov", $_POST['nazov'], $errorNazovSTR);
    $errorCena = $db->filterCena("cena", $_POST['cena'], $errorCenaSTR);
    $errorPopis = $db->filterEmpty("popis", $_POST['popis'], $errorPopisSTR);
    $errorObrazok = $db->filterObrazok("obrazok", $_POST['imgURL'], $errorObrazokSTR);

    /*var_dump($errorNazov);
    var_dump($errorCena);
    var_dump($errorPopis);
    var_dump($errorObrazok);

    var_dump($errorNameSTR);
    var_dump($errorCenaSTR);
    var_dump($errorPopisSTR);
    var_dump($errorObrazokSTR);*/

    if(!$errorNazov && !$errorCena && !$errorPopis && !$errorObrazok) {
        $db->addOblecenie($_POST);
        header("Location: http://localhost/semestralka/");
    }
}

?>
    <form action="addOblecenie.php" method="post">
        <div class="form-input">
            <label for="nazov">Názov</label>
            <input type="text" id="nazov" name="nazov" placeholder="Názov">
            <p style="color: red;"><?php echo $errorNazovSTR; ?></p>
        </div>
        <div class="form-input">
            <label for="cena">Cena</label>
            <input type="number" step="0.01" id="cena" name="cena" placeholder="Cena">
            <p style="color: red;"><?php echo $errorCenaSTR; ?></p>
        </div>
        <div class="form-input">
            <label for="imgURL">Adresa obrázku</label>
            <input type="text" id="imgURL" name="imgURL" placeholder="Adresa obrázku">
            <p style="color: red;"><?php echo $errorObrazokSTR; ?></p>
        </div>
        <div class="form-input">
            <label for="popis">Popis</label>
            <input type="text" id="popis" name="popis" placeholder="Popis">
            <p style="color: red;"><?php echo $errorPopisSTR; ?></p>
        </div>
        <div class="form-input">
            <button type="submit">Pridať</button>
        </div>
    </form>
<?php
include_once('includes/footer.html');
?>